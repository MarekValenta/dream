﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using static MyUtil;

public class ChunkTest {
    // A Test behaves as an ordinary method

    [Test]
    public void createSimpleChunkTest() {
        ChunkHeightGenerator chunkHeightGenerator = new ChunkHeightGenerator();
        var indexOfChunk = Index.getIndex(0, 0, 0);
        var generateHeights = chunkHeightGenerator.generateHeights(TYPE_GENERATOR, indexOfChunk);
        Chunk chunk = new Chunk(TYPE_GENERATOR, indexOfChunk, generateHeights);
        var meshData = chunk.getMeshData();

        Assert.AreEqual(1232, meshData.vertices.Count);
        Assert.AreEqual(1848, meshData.triangles.Count);
        Assert.AreEqual(1232, meshData.uv.Count);
    }

    [Test]
    public void createVerticalChunksTest() {
        ChunkHeightGenerator chunkHeightGenerator = new ChunkHeightGenerator();

        List<MyMesh> meshes = new List<MyMesh>();
        var generateHeights = chunkHeightGenerator.generateHeights(TYPE_GENERATOR, Index.getIndex(0, 0, 0));
        for (int y = -2; y < 6; y++) {
            var indexOfChunk = Index.getIndex(0, y, 0).multiply(16);
            Chunk chunk = new Chunk(TYPE_GENERATOR, indexOfChunk, generateHeights);
            meshes.Add(chunk.getMeshData());
        }

        Assert.AreEqual(8, meshes.Count);

        Assert.IsEmpty(meshes[0].vertices);
        Assert.IsEmpty(meshes[0].triangles);
        Assert.IsEmpty(meshes[0].uv);

        Assert.IsEmpty(meshes[1].vertices);
        Assert.IsEmpty(meshes[1].triangles);
        Assert.IsEmpty(meshes[1].uv);

        Assert.AreEqual(1232, meshes[2].vertices.Count);
        Assert.AreEqual(1848, meshes[2].triangles.Count);
        Assert.AreEqual(1232, meshes[2].uv.Count);

        Assert.AreEqual(16, meshes[3].vertices.Count);
        Assert.AreEqual(24, meshes[3].triangles.Count);
        Assert.AreEqual(16, meshes[3].uv.Count);
    }

    [Test]
    public void loadChunksTest() {
        ChunkHeightGenerator chunkHeightGenerator = new ChunkHeightGenerator();
        List<MyMesh> meshes = new List<MyMesh>();
        var xSize = 20;
        var zSize = 20;
        for (int x = 0; x < xSize; x++) {
            for (int z = 0; z < zSize; z++) {
                var generateHeights = chunkHeightGenerator.generateHeights(TYPE_GENERATOR, Index.getIndex(x, 0, z).multiply(16));
                for (int y = -2; y < 6; y++) {
                    var indexOfChunk = Index.getIndex(z, y, z).multiply(16);
                    Chunk chunk = new Chunk(TYPE_GENERATOR, indexOfChunk, generateHeights);
                    meshes.Add(chunk.getMeshData());
                }
            }
        }

        Assert.AreEqual(xSize * zSize * 8, meshes.Count);
    }

    [Test]
    public void loadEmptyChunksTest() {
        var generateHeights = new float[SIZE_OF_CHUNK, SIZE_OF_CHUNK];

        for (int x = 0; x < SIZE_OF_CHUNK; x++) {
            for (int z = 0; z < SIZE_OF_CHUNK; z++) {
                generateHeights[x, z] = SIZE_OF_CHUNK * 2;
            }
        }

        for (int x = 0; x < 3*20*20*8; x++) {
                var indexOfChunk = Index.getIndex(0, 0, 0).multiply(16);
                Chunk chunk = new Chunk(TYPE_GENERATOR, indexOfChunk, generateHeights);
        }
    }
}