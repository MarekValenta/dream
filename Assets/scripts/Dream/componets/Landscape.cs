using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static MyUtil;

public class Landscape : MonoBehaviour {
    public GameObject chunk;
    public GameObject VerticalChunk;
    public GameObject ChunksParent;
    public Camera mainCamera;
    public LayerMask mask;
    private int size = 16;


    private ConcurrentDictionary<int, GameObjectWithComponent<VerticalChunks>> viewObjectsD =
        new ConcurrentDictionary<int, GameObjectWithComponent<VerticalChunks>>();

    private int numberOfVerticalChunksInMemory = 1000;

    private Stack<GameObjectWithComponent<VerticalChunks>> verticalChunks =
        new Stack<GameObjectWithComponent<VerticalChunks>>();

    private Index _cameraPosition;
    private Quaternion _cameraRotation;
    private ChunkProvider _chunkProvider = new ChunkProvider();

    CameraViewPoints cameraViewPoints;

    private void Start() {
        initVerticalChunks();

        cameraViewPoints = new CameraViewPoints(mainCamera, mask);
        cameraViewPoints.calculatePoints();
        var pointsForCreating = cameraViewPoints.getPointsForCreating();
        _chunkProvider.generateChunks(pointsForCreating, -2, 6);
        activateChunks(pointsForCreating);
        _cameraPosition = Index.@from(mainCamera.transform.position);
        _cameraRotation = mainCamera.transform.rotation;
    }

    private void Update() {
        var cameraPosition = Index.@from(mainCamera.transform.position);
        var cameraRotaion = mainCamera.transform.rotation;
        var quaternion = cameraRotaion * Quaternion.Inverse(_cameraRotation);
        if (!cameraPosition.isEqual(_cameraPosition) || tooMuchRotaion(quaternion)) {
            cameraViewPoints.calculatePoints();

            List<Index> deleteChunk = cameraViewPoints.getPointsForDeleting();
            List<Index> createChunks = cameraViewPoints.getPointsForCreating();

            delteChunks(deleteChunk);
            activateChunks(createChunks);

            _chunkProvider.deleteChunks(deleteChunk, -2, 6);
            _chunkProvider.generateChunks(createChunks, -2, 6);
            _cameraRotation = mainCamera.transform.rotation;
        }

        _cameraPosition = Index.@from(mainCamera.transform.position);
    }

    private static bool tooMuchRotaion(Quaternion quaternion) {
        return quaternion.x > 0.1f || quaternion.x < -0.1f || quaternion.y > 0.1f || quaternion.y < -0.1f;
    }

    private void activateChunks(List<Index> pointsLocal) {
        for (var index = 0; index < pointsLocal.Count; index++) {
            var point = pointsLocal[index];
            var gameObjectWithComponent = verticalChunks.Pop();
            gameObjectWithComponent.gameObject.transform.position = new Vector3(point.x * size, 0, point.z * size);
            gameObjectWithComponent.gameObject.SetActive(true);
            gameObjectWithComponent.component.activate();
            viewObjectsD.TryAdd(hash(point.x, 0, point.z), gameObjectWithComponent);
        }
    }

    private void delteChunks(List<Index> pointsLocal) {
        for (var index = 0; index < pointsLocal.Count; index++) {
            var point = pointsLocal[index];
            viewObjectsD.TryRemove(hash(point.x, 0, point.z), out var verticalChunk);
            if (verticalChunk != null) {
                verticalChunk.component.deActivate();
                verticalChunk.gameObject.SetActive(false);
                verticalChunks.Push(verticalChunk);
            }
        }
    }

    private void initVerticalChunks() {
        for (int i = 0; i < numberOfVerticalChunksInMemory; i++) {
            var tempChunk = Instantiate(VerticalChunk, ChunksParent.transform);
            tempChunk.SetActive(false);
            var verticalChunksComponent = tempChunk.GetComponent<VerticalChunks>();
            verticalChunksComponent.mainCamera = mainCamera;
            verticalChunksComponent.chunkProvider = _chunkProvider;
            verticalChunksComponent.deActivate();
            verticalChunksComponent.initChunks();

            verticalChunks.Push(new GameObjectWithComponent<VerticalChunks>(tempChunk, verticalChunksComponent));
        }
    }
}