using System;
using UnityEngine;
using UnityEngine.Serialization;

public class Selector : MonoBehaviour {
    public GameObject SelectorPrefab;
    public Camera mainCamera;
    private GameObject _selectorObject;

    private void Start() {
        _selectorObject = Instantiate(SelectorPrefab);
        _selectorObject.SetActive(false);
    }


    private void Update() {
            if (!Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit, 1000.0f)) {
                _selectorObject.SetActive(false);
                return;
            }
            if (hit.collider.transform.name.Contains("Selector")) {
                return;
            }


            Vector3 pos;
            if (Input.GetKey(KeyCode.LeftAlt)) {
                pos = Index.@from(hit.point + hit.normal / 2).vector3() + new Vector3(0.5f,0.5f,0.5f);
            } else {
                pos = Index.@from(hit.point - hit.normal / 2).vector3() + new Vector3(0.5f,0.5f,0.5f);
                
            }
            _selectorObject.transform.position = pos;
            _selectorObject.SetActive(true);
        
    }
}