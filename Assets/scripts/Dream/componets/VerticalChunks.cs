using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalChunks : MonoBehaviour {
    public Camera mainCamera { get; set; }
    public ChunkProvider chunkProvider { get; set; }

    public GameObject _chunk;

    private int minY = -2;
    private int maxY = 6;

    private List<GameObjectWithComponent<ChunkComponent>> chunks = new List<GameObjectWithComponent<ChunkComponent>>();
    
    public void initChunks() {
        for (int i = minY; i < maxY; i++) {
            var tempChunk = Instantiate(_chunk, transform);
            var chunkComponent = tempChunk.GetComponent<ChunkComponent>();
            chunks.Add(new GameObjectWithComponent<ChunkComponent>(tempChunk, chunkComponent));
        }
    }
    

    public void generateChunks(Index indexOfChunk) {
        StartCoroutine(generateChunksCorutine(indexOfChunk));
    }

    private IEnumerator generateChunksCorutine(Index indexOfChunk) {
        var y = minY;
        foreach (var gameObjectWithComponent in chunks) {
            gameObjectWithComponent.gameObject.transform.position =
                new Vector3(indexOfChunk.x, y * MyUtil.SIZE_OF_CHUNK, indexOfChunk.z);
            var ofChunk = Index.@from(gameObjectWithComponent.gameObject.transform.position);
            Chunk chunk = null;
            yield return new WaitUntil(() => chunkProvider.getChunk(ofChunk, out chunk));
            gameObjectWithComponent.component.init(chunk);
            y++;
        }
    }

    public void deActivate() {
        foreach (var gameObjectWithComponent in chunks) {
            gameObjectWithComponent.component.deActivate();
        }
    }

    public void activate() {
        generateChunks(Index.@from(transform.position));
    }
}