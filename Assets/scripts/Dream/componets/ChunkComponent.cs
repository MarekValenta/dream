using System;
using System.Collections;
using System.Threading;
using UnityEngine;
using Random = System.Random;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class ChunkComponent : MonoBehaviour {
    private MeshFilter meshFilter;
    private MeshCollider meshCollider;

    private void Awake() {
        meshFilter = GetComponent<MeshFilter>();
        meshCollider = GetComponent<MeshCollider>();
    }

    public void init(Chunk chunk) {
        setComponents(chunk);
    }

    private void setComponents(Chunk chunk) {
        Mesh mesh = new Mesh {name = "chunkMesh"};
        mesh.Clear();
        mesh.vertices = chunk.MeshData.vertices.ToArray();
        mesh.triangles = chunk.MeshData.triangles.ToArray();
        mesh.uv = chunk.MeshData.uv.ToArray();
        mesh.Optimize();
        mesh.RecalculateNormals();
        meshFilter.sharedMesh = mesh;
        meshCollider.sharedMesh = meshFilter.sharedMesh;
    }

    private void OnDestroy() {
        Destroy(meshCollider);
        Destroy(meshFilter);
    }

    public void deActivate() {
        meshFilter.mesh.Clear();
    }
}