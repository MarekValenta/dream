using System;
using System.Collections.Generic;
using System.Linq;
using Dream;
using UnityEngine;
using Random = System.Random;

public class Chunk {
    private BlockArray Blocks { get; }
    private readonly BaseBlockProviderGenerator baseBlockTypeGenerator;
    private readonly Index indexOfChunk;
    public MyMesh MeshData { get; }

    public Chunk(BaseBlockProviderGenerator baseBlockProviderGenerator, Index indexOfChunk,
        float[,] heights) {
        this.indexOfChunk = indexOfChunk;
        baseBlockTypeGenerator = baseBlockProviderGenerator;
        Blocks = new BlockArray(MyUtil.SIZE_OF_CHUNK);

        if (doesChunkNeedToBeGenerated(heights)) {
            MyUtil.forEachBlockOnThePlane(index => { createBlocks(index, heights); }, MyUtil.SIZE_OF_CHUNK);
        }

        MeshData = getMeshData();
    }

    private bool doesChunkNeedToBeGenerated(float[,] heights) {
        bool render = false;
        MyUtil.forEachBlockOnThePlane(index => {
            float maxHeight = heights[index.x, index.z];
            if (!render && maxHeight - indexOfChunk.y >= -1 && maxHeight - indexOfChunk.y < MyUtil.SIZE_OF_CHUNK) {
                render = true;
                return false;
            }

            return true;
        }, MyUtil.SIZE_OF_CHUNK);
        return render;
    }

    private void createBlock(Index index, SideE side) {
        var block = Blocks.getBlock(index);
        if (block == null) {
            var generateType = baseBlockTypeGenerator.generateType(index, indexOfChunk);
            if (generateType != BlockType.Air) {
                Blocks.setBlock(index,
                    new Block(index, generateType, side));
            }
        } else {
            block.setVisibleSide(side);
        }
    }

    private void createBlocks(Index index2, float[,] heights) {
        int height1 = (int) Math.Floor(heights[index2.x, index2.z]);

        int heightLeft;
        if (index2.x == 0) {
            heightLeft = (int) Math.Floor(baseBlockTypeGenerator.getMaxHeight(index2.left(), indexOfChunk));
        } else {
            heightLeft = (int) Math.Floor(heights[index2.x - 1, index2.z]);
        }

        int heightFront;
        if (index2.z == 0) {
            heightFront = (int) Math.Floor(baseBlockTypeGenerator.getMaxHeight(index2.front(), indexOfChunk));
        } else {
            heightFront = (int) Math.Floor(heights[index2.x, index2.z - 1]);
        }

        int heightRight;
        if (index2.x >= MyUtil.SIZE_OF_CHUNK - 1) {
            heightRight = (int) Math.Floor(baseBlockTypeGenerator.getMaxHeight(index2.right(), indexOfChunk));
        } else {
            heightRight = (int) Math.Floor(heights[index2.x + 1, index2.z]);
        }

        int heightBack;
        if (index2.z >= MyUtil.SIZE_OF_CHUNK - 1) {
            heightBack = (int) Math.Floor(baseBlockTypeGenerator.getMaxHeight(index2.back(), indexOfChunk));
        } else {
            heightBack = (int) Math.Floor(heights[index2.x, index2.z + 1]);
        }


        height1 -= indexOfChunk.y;
        if (height1 >= 0 && height1 < MyUtil.SIZE_OF_CHUNK) {
            createBlock(Index.getIndex(index2.x, height1, index2.z), SideE.Top);
        }

        getSideBlocks(index2, heightLeft, height1, SideE.Left);
        getSideBlocks(index2, heightFront, height1, SideE.Front);
        getSideBlocks(index2, heightBack, height1, SideE.Back);
        getSideBlocks(index2, heightRight, height1, SideE.Right);
    }

    private void getSideBlocks(Index index2, int height, int height1, SideE side) {
        if (height1 >= MyUtil.SIZE_OF_CHUNK) {
            height1 = MyUtil.SIZE_OF_CHUNK - 1;
        }

        height -= indexOfChunk.y;
        if (height < 0) {
            height = -1;
        }

        if (height1 <= height) return;
        for (int y = height + 1; y <= height1; y++) {
            createBlock(Index.getIndex(index2.x, y, index2.z), side);
        }
    }

    public MyMesh getMeshData() {
        MyMesh mesh = new MyMesh();
        var polygons = new List<Polygon>();
        foreach (var block in Blocks.BlocksGenerated) {
            polygons.AddRange(block.generateMesh());
        }

        var vertices = new List<Vector3>();
        var triangles = new List<int>();
        var uv = new List<Vector2>();

        var sizeOfTriangles = 0;
        foreach (var polygon in polygons) {
            vertices.AddRange(polygon.vertices);
            uv.AddRange(polygon.uv);
            foreach (var polygonTriangle in polygon.triangles) {
                triangles.Add(polygonTriangle + sizeOfTriangles);
            }

            sizeOfTriangles += polygon.vertices.Length;
        }

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uv;

        return mesh;
    }
}