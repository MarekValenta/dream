using System;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using Dream;
using UnityEngine;

public class Polygon {
    private static bool isInit;

    private static readonly int[] topTriangles = {1, 0, 3, 1, 3, 2};
    private static readonly int[] bottomTriangles = {0, 2, 3, 0, 1, 2};
    private static readonly int[] leftTriangles = {0, 1, 2, 0, 2, 3};
    private static readonly int[] rightTriangles = {0, 3, 2, 0, 2, 1};
    private static readonly int[] frontTriangles = {0, 2, 1, 0, 3, 2};
    private static readonly int[] backTriangles = {2, 3, 0, 2, 0, 1};

    private static readonly Vector2[] uvGreenSide = {
        new Vector2(1f / 16f * 3f, 1f / 16f * 15f), new Vector2(1f / 16f * 4f, 1f / 16f * 15f),
        new Vector2(1f / 16f * 4f, 1f), new Vector2(1f / 16f * 3f, 1)
    };

    private static readonly Vector2[] uvGreenTop = {
        new Vector2(1f / 16f * 2f, 1f / 16f * 6f), new Vector2(1f / 16f * 3f, 1f / 16f * 6f),
        new Vector2(1f / 16f * 3f, 1f / 16f * 7f), new Vector2(1f / 16f * 2f, 1f / 16f * 7f)
    };

    private static readonly Vector2[] uvStone = {
        new Vector2(0, 1f / 16f * 14f), new Vector2(1f / 16f, 1f / 16f * 14f),
        new Vector2(1f / 16f, 1f / 16f * 15f), new Vector2(0, 1f / 16f * 15f)
    };

    private static readonly Vector2[] uvStoneTop = {
        new Vector2(1f / 16f * 2f, 1f / 16f * 6f), new Vector2(1f / 16f * 3f, 1f / 16f * 6f),
        new Vector2(1f / 16f * 3f, 1f / 16f * 7f), new Vector2(1f / 16f * 2f, 1f / 16f * 7f)
    };

    private static readonly Vector3[] vc = {
        new Vector3(0, 0, 0),
        new Vector3(1, 0, 0),
        new Vector3(1, 1, 0),
        new Vector3(0, 1, 0),
        new Vector3(0, 1, 1),
        new Vector3(1, 1, 1),
        new Vector3(1, 0, 1),
        new Vector3(0, 0, 1),
    };

    private static readonly Polygon[,,,,] polygons = initPolygons();


    public static Polygon getPolygon(SideE side, Index i, BlockType type) {
        return polygons[(int) side, (int) type, i.x, i.y, i.z];
    }

    private static Polygon[,,,,] initPolygons() {
        Polygon[,,,,] tempPolygons = new Polygon[6, 2, 16, 16, 16];
        foreach (var allSide in Side.AllSides()) {
            MyUtil.forEachXYZ((x, y, z) => {
                var index = Index.getIndex(x, y, z);
                foreach (var value in new []{BlockType.Dirt, BlockType.Stone}) {
                    tempPolygons[allSide, (int) value, x, y, z] = new Polygon((SideE) allSide, index, value);
                }
            }, 16, 16, 16);
        }

        isInit = true;
        return tempPolygons;
    }

    public Vector3[] vertices { get; }
    public int[] triangles { get; }

    public Vector2[] uv { get; }


    private Polygon(SideE side, Index i, BlockType type) {
        switch (side) {
            case SideE.Top:
                vertices = new[] {vc[3] + i.vector3(), vc[2] + i.vector3(), vc[5] + i.vector3(), vc[4] + i.vector3()};
                triangles = topTriangles;
                uv = type switch {
                    BlockType.Dirt => uvGreenTop,
                    BlockType.Stone => uvStone,
                    _ => uv
                };

                break;
            case SideE.Bottom:
                vertices = new[] {vc[0] + i.vector3(), vc[1] + i.vector3(), vc[6] + i.vector3(), vc[7] + i.vector3()};
                triangles = bottomTriangles;
                uv = new[] {new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0)};
                break;
            case SideE.Left:
                vertices = new[] {vc[0] + i.vector3(), vc[7] + i.vector3(), vc[4] + i.vector3(), vc[3] + i.vector3()};
                triangles = leftTriangles;
                uv = type switch {
                    BlockType.Dirt => uvGreenSide,
                    BlockType.Stone => uvStone,
                    _ => uv
                };
                break;
            case SideE.Right: // 1, 2, 5, 1, 5, 6,
                vertices = new[] {vc[1] + i.vector3(), vc[6] + i.vector3(), vc[5] + i.vector3(), vc[2] + i.vector3()};
                triangles = rightTriangles;
                uv = type switch {
                    BlockType.Dirt => uvGreenSide,
                    BlockType.Stone => uvStone,
                    _ => uv
                };
                break;
            case SideE.Front:
                vertices = new[] {vc[0] + i.vector3(), vc[1] + i.vector3(), vc[2] + i.vector3(), vc[3] + i.vector3()};
                triangles = frontTriangles;
                uv = type switch {
                    BlockType.Dirt => uvGreenSide,
                    BlockType.Stone => uvStone,
                    _ => uv
                };
                break;
            case SideE.Back:
                vertices = new[] {vc[7] + i.vector3(), vc[6] + i.vector3(), vc[5] + i.vector3(), vc[4] + i.vector3()};
                triangles = backTriangles;
                uv = type switch {
                    BlockType.Dirt => uvGreenSide,
                    BlockType.Stone => uvStone,
                    _ => uv
                };
                break;
        }
    }
}