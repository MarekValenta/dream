using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using JetBrains.Annotations;
using Unity.Profiling;
using UnityEngine;
using static MyUtil;

public class ChunkProvider {
    private static ChunkProvider chunkProviderToContext = addToContext<ChunkProvider>(typeof(ChunkProvider));
    private readonly ConcurrentDictionary<int, Chunk> chunks = new ConcurrentDictionary<int, Chunk>();
    private const int NUMBER_OF_THREAD = 8;

    private readonly ChunkHeightGenerator chunkHeightGenerator = new ChunkHeightGenerator();

    public ChunkProvider() {
        ThreadPool.SetMaxThreads(NUMBER_OF_THREAD, NUMBER_OF_THREAD);
        ThreadPool.SetMinThreads(NUMBER_OF_THREAD, NUMBER_OF_THREAD);
    }

    public void generateChunks(List<Index> indexes, int heightStart, int heightStop) {
        var numberOfParallelCalculations = Math.Max(1, Math.Min(indexes.Count / 20, NUMBER_OF_THREAD));
        // numberOfParallelCalculations = 1;

        for (int i = 0; i < numberOfParallelCalculations; i++) {
            var i1 = i;
            ThreadPool.QueueUserWorkItem(state => {
                parallelCalculation(indexes, (int) (indexes.Count / (float) numberOfParallelCalculations * i1),
                    (int) ((indexes.Count / (float) numberOfParallelCalculations) * (i1 + 1)), heightStart, heightStop);
            });
        }
    }

    public void deleteChunks(List<Index> pointsTemp, int heightStart, int heightStop) {
        ThreadPool.QueueUserWorkItem(state => {
            foreach (var index in pointsTemp) {
                for (int y = heightStart; y < heightStop; y++) {
                    chunks.TryRemove(hash(index.x, y, index.z), out _);
                }
            }
        });
    }


    private void parallelCalculation(List<Index> indexes, int startX, int endX, int heightStart, int heightStop) {
        for (int x = startX; x < endX; x++) {
            var index = indexes[x];
            var indexOfChunk = Index.getIndex(index.x * SIZE_OF_CHUNK, 0, index.z * SIZE_OF_CHUNK);
            var heights = chunkHeightGenerator.generateHeights(TYPE_GENERATOR, indexOfChunk);
            for (int y = heightStart; y < heightStop; y++) {
                indexOfChunk = Index.getIndex(index.x * SIZE_OF_CHUNK, y * SIZE_OF_CHUNK,
                    index.z * SIZE_OF_CHUNK);
                var tryAdd = chunks.TryAdd(hash(index.x, y, index.z), new Chunk(TYPE_GENERATOR, indexOfChunk,
                    heights));
            }
        }
    }

    public bool getChunk(Index index, out Chunk value) {
        return chunks.TryGetValue(
            hash(index.x / SIZE_OF_CHUNK, index.y / SIZE_OF_CHUNK, index.z / SIZE_OF_CHUNK),
            out value);
    }
}