using static MyUtil;

public class ChunkHeightGenerator {
    
    public float[,] generateHeights(BaseBlockProviderGenerator baseBlockProviderGenerator,
        Index indexOfChunk) {
        float[,] heights = new float[SIZE_OF_CHUNK, SIZE_OF_CHUNK];
        for (int x = 0; x < SIZE_OF_CHUNK; x++) {
            for (int z = 0; z < SIZE_OF_CHUNK; z++) {
                var index = Index.getIndex(x, 0, z);
                var maxHeight = baseBlockProviderGenerator.getMaxHeight(index, indexOfChunk);
                heights[x, z] = maxHeight;
            }
        }

        return heights;
    }
}