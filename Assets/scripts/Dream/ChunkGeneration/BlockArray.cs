using System.Collections.Generic;
using UnityEngine;

public class BlockArray {
    private Block[,,] Blocks { get; }
    public List<Block> BlocksGenerated { get; }
    
    public BlockArray(int size) {
        Blocks = new Block[size,size,size];
        BlocksGenerated = new List<Block>(size*size*size);
    }

    public Block getBlock(Index index) {
        // if (index.isOutsideChunk(16)) {
        //     Debug.Log("");
        // }
        return Blocks[index.x, index.y, index.z];
    }

    public void setBlock(Index index, Block block) {
        Blocks[index.x, index.y, index.z] = block;
        BlocksGenerated.Add(block);
    }
}