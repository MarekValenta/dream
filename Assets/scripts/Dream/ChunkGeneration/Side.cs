using System;
using Dream;

public class Side {

    public static SideE[] allSidesExceptBottomAndTop = {SideE.Left, SideE.Right, SideE.Front, SideE.Back};

    public static int[] AllSides() {
        return new[] {
            (int) SideE.Top, (int) SideE.Bottom, (int) SideE.Left, (int) SideE.Right, (int) SideE.Front,
            (int) SideE.Back
        };
    }

    public static SideE oppositeSide(SideE side) {
        switch (side) {
            case SideE.Top: // bottom
                return SideE.Bottom;
            case SideE.Bottom: // top
                return SideE.Top;
            case SideE.Left: // right
                return SideE.Right;
            case SideE.Right: // left
                return SideE.Left;
            case SideE.Front: // back
                return SideE.Back;
            case SideE.Back: //front
                return SideE.Front;
        }
        throw new Exception("unrecognized side");
    }
}