using System.Collections.Generic;
using UnityEngine;

public class MyMesh {
    public List<Vector3> vertices { get; set; }
    public List<Vector2> uv { get; set; }
    public List<int> triangles { get; set; }
}