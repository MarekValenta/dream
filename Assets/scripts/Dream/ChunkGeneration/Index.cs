using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Dream;
using JetBrains.Annotations;
using UnityEngine;

public class Index {
    private static readonly Index[,,] indexes = initIndexes();
    private static bool isInit;
    private const int SIZE = 16;
    public int x { get; }
    public int y { get; }
    public int z { get; }

    private static Index[,,] initIndexes() {
        var dictionary = new Index[SIZE, SIZE, SIZE];
        MyUtil.forEachXYZ((x, y, z) => dictionary[x, y, z] = new Index(x, y, z), SIZE, SIZE, SIZE);
        isInit = true;
        return dictionary;
    }

    public static Index getIndex(int x, int y, int z) {

        if (x < SIZE && y < SIZE && z < SIZE && x >= 0 && y >= 0 && z >= 0) {
            return indexes[x, y, z];
        }

        return new Index(x, y, z);
    }

    private Index(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    private static int getHash(int x, int y, int z) {
        return x + y * 100 + z * 100 * 100;
    }

    public static Index from(Vector3 vector3) {
        return getIndex((int) vector3.x, (int) vector3.y, (int) vector3.z);
    }

    public Index divide(int number) {
        return getIndex(x / number, y / number, z / number);
    }

    public Index multiply(int number) {
        return getIndex(x * number, y * number, z * number);
    }
    
    public Index multiply(float number) {
        return getIndex((int) (x * number), (int) (y * number), (int) (z * number));
    }

    public Index minus(Index index) {
        return getIndex(x - index.x, y - index.y, z - index.z);
    }

    public Vector3 vector3() {
        return new Vector3(x, y, z);
    }

    public Index add(Index index) {
        return getIndex(index.x + x, index.y + y, index.z + z);
    }

    public Index down() {
        return getIndex(x, y - 1, z);
    }

    public Index left() {
        return getIndex(x - 1, y, z);
    }

    public Index right() {
        return getIndex(x + 1, y, z);
    }

    public Index top() {
        return getIndex(x, y + 1, z);
    }

    public Index back() {
        return getIndex(x, y, z + 1);
    }

    public Index front() {
        return getIndex(x, y, z - 1);
    }

    public Index side(SideE side) {
        Index index = null;
        switch (side) {
            case SideE.Top:
                index = top();
                break;
            case SideE.Bottom:
                index = down();
                break;
            case SideE.Left:
                index = left();
                break;
            case SideE.Right:
                index = right();
                break;
            case SideE.Front:
                index = front();
                break;
            case SideE.Back:
                index = back();
                break;
        }

        return index;
    }

    public bool isOutsideChunk(int size) {
        return x >= size || y >= size || z >= size || x < 0 || y < 0 || z < 0;
    }

    public Index resetToSize(int size) {
        if (x >= size) {
            return getIndex(0, y, z);
        }

        if (x < 0) {
            return getIndex(15, y, z);
        }

        if (y >= size) {
            return getIndex(x, 0, z);
        }

        if (y < 0) {
            return getIndex(x, 15, z);
        }

        if (z >= size) {
            return getIndex(x, y, 0);
        }

        if (z < 0) {
            return getIndex(x, y, 15);
        }

        throw new Exception("resetToSize wasnt outside chunk");
    }

    public Index isBiggerThanSizeGetChunk(int size) {
        if (x >= size) {
            return getIndex(16, 0, 0);
        }

        if (x < 0) {
            return getIndex(-16, 0, 0);
        }

        if (y >= size) {
            return getIndex(0, 16, 0);
        }

        if (y < 0) {
            return getIndex(0, -16, 0);
        }

        if (z >= size) {
            return getIndex(0, 0, 16);
        }

        if (z < 0) {
            return getIndex(0, 0, -16);
        }

        return null;
    }

    public Index side(SideE side, int size) {
        Index index = this.side(side);

        if (index.x < size && index.y < size && index.z < size) {
            return index;
        }

        return null;
    }

    public bool isEqual(Index index) {
        if (index == null) {
            return false;
        }

        return x == index.x && y == index.y && z == index.z;
    }

    public override string ToString() {
        return "x: " + x + ", y: " + y + ", z: " + z;
    }
}