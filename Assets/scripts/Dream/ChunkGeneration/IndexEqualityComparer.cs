using System;
using System.Collections.Generic;

public class IndexEqualityComparer : IEqualityComparer<Index> {
    public bool Equals(Index x, Index y) {
        return x.x == y.x && x.y == y.y && x.z == y.z;
    }

    public int GetHashCode(Index obj) {
        return hash(obj.x, obj.y, obj.z);
    }

    private int hash(int x, int y, int z) {
        if (x == 0 && y == 0 && z == 0) {
            return Int32.MaxValue;
        }

        return (((x << 8) ^ y) << 8) ^ z;
    }
}
