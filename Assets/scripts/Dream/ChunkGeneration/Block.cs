using System;
using System.Collections.Generic;
using Dream;
using UnityEngine;

public class Block {
    public BlockType Type { get; }
    private readonly Index index;
    private readonly bool[] visibleSides = {false, false, false, false, false, false};
    private int polygonsCount;

    public Block(Index index, BlockType type, SideE side) {
        Type = type;
        this.index = index;
        visibleSides[(int) side] = true;
        polygonsCount++;
    }

    public void setVisibleSide(SideE side) {
        visibleSides[(int) side] = true;
        polygonsCount++;
    }

    public Polygon[] generateMesh() {
        Polygon[] polygons = new Polygon[polygonsCount];
        int ii = 0;
        for (var i = 0; i < visibleSides.Length; i++) {
            if (visibleSides[i]) {
                var side = (SideE) i;
                polygons[ii] = Polygon.getPolygon(side, index, Type);
                ii++;
            }
        }
        return polygons;
    }
}