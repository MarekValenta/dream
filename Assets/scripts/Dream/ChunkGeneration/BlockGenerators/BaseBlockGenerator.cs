using System.Collections.Generic;
using UnityEngine;

public abstract class BaseBlockProviderGenerator {
    protected abstract float terrainFunction(Index index);
    protected abstract float stoneFunction(Index index);
    public abstract BlockType generateType(Index index, Index indexOfChunk);

    public float getMaxHeight(Index index2, Index indexOfChunk) {
        Index index = index2.add(indexOfChunk);
        return terrainFunction(index);
    }
    
    public float getMaxHeightStone(Index index2, Index indexOfChunk) {
        Index index = index2.add(indexOfChunk);
        return stoneFunction(index);
    }

}