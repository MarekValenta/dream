using System;
using System.Collections.Generic;
using UnityEngine;

public class BaseBlockTypeGenerator : BaseBlockProviderGenerator {

    public override BlockType generateType(Index index2, Index indexOfChunk) {
        Index index = index2.add(indexOfChunk);
        var height = getMaxHeight(index2, indexOfChunk);
        if (index.y <= height) {
            return BlockType.Dirt;
        }

        return BlockType.Air;
    }

    protected override float stoneFunction(Index index) {
        return (index.x + index.z) - 2;
    }


    protected override float terrainFunction(Index index) {
        return (index.x + index.z);
    }

    
    
}