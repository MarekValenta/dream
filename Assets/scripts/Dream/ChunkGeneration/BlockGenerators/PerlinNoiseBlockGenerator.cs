using UnityEngine;
using UnityEngine.UIElements;
using Random = System.Random;

public class PerlinNoiseBlockGenerator : BaseBlockProviderGenerator {
    //int x, int y, int z, float scale, float height, float power
    // int stone = PerlinNoise(x, 0, z, 20, 3, 1.2f);
    private static FastNoise fastNoise = new FastNoise();
    private static FastNoise stoneFastNoise = new FastNoise(420);
    Random random = new Random();

    public PerlinNoiseBlockGenerator() {
        fastNoise.SetFrequency(0.003f);
        stoneFastNoise.SetFrequency(0.002f);
        stoneFastNoise.SetFractalType(FastNoise.FractalType.RigidMulti);
    }

    protected override float terrainFunction(Index index) {
        int height = 5 * 16;
        var perlinFractal = fastNoise.GetPerlinFractal(index.x, index.z);
        perlinFractal *= height;

        return perlinFractal + 16;
    }

    protected override float stoneFunction(Index index) {
        int height = 4 * 16;
        var perlinFractal = stoneFastNoise.GetPerlinFractal(index.x, index.z);
        perlinFractal *= height;

        return perlinFractal;
    }
    
    public override BlockType generateType(Index index2, Index indexOfChunk) {
        Index index = index2.add(indexOfChunk);
        var height = getMaxHeight(index2, indexOfChunk);
        var heightStone = getMaxHeightStone(index2, indexOfChunk);
        if (index.y <= height) {
            if (index.y <= heightStone) {
                return BlockType.Stone;
            }

            return BlockType.Dirt;
        }

        return BlockType.Air;
    }
}