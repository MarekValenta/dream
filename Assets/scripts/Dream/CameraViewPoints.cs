using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static MyUtil;

public class CameraViewPoints {
    private List<Index> lastPoints;
    public List<Index> points;
    private CameraFieldOfViewCorners _cameraFieldOfViewCorners;

    public CameraViewPoints(Camera camera, LayerMask mask) {
        _cameraFieldOfViewCorners = new CameraFieldOfViewCorners(camera, mask);
    }

    public List<Index> getPointsForCreating() {
        if (lastPoints == null) {
            return points;
        }
        return points.Except(lastPoints, new IndexEqualityComparer()).ToList();
    }
    
    public List<Index> getPointsForDeleting() {
        return lastPoints.Except(points, new IndexEqualityComparer()).ToList();
    }
    
    public void calculatePoints() {
        if (points != null) {
            lastPoints = points;
        }
        points = getPointsTest(_cameraFieldOfViewCorners.getCorners());
    }

    public List<Index> getPointsTest(Index[] corners) {
        var orderedZ = corners.OrderBy(index => index.z).ToArray();

        var pointsOnLine = GetPointsOnLine(corners[0].x, corners[0].z, corners[1].x, corners[1].z)
            .OrderBy(v => v.y).ToList();
        var pointsOnLine2 = GetPointsOnLine(corners[1].x, corners[1].z, corners[2].x, corners[2].z)
            .OrderBy(v => v.y).ToList();
        var pointsOnLine3 = GetPointsOnLine(corners[2].x, corners[2].z, corners[3].x, corners[3].z)
            .OrderBy(v => v.y).ToList();
        var pointsOnLine4 = GetPointsOnLine(corners[3].x, corners[3].z, corners[0].x, corners[0].z)
            .OrderBy(v => v.y).ToList();

        pointsOnLine.AddRange(pointsOnLine2);
        pointsOnLine.AddRange(pointsOnLine3);
        pointsOnLine.AddRange(pointsOnLine4);

        var dictionary = pointsOnLine.GroupBy(v => v.y).ToDictionary(vector2S => vector2S.Key);

        List<Index> points = new List<Index>();
        for (int i = orderedZ[0].z; i <= orderedZ[3].z; i++) {
            var lowest = getLowest(dictionary[i].ToArray(), index => index.x);
            var highest = getHighest(dictionary[i].ToArray(), index => index.x);
            for (int x = (int) lowest.x; x <= highest.x; x++) {
                points.Add(Index.getIndex(x, 0, i));
            }
        }


        return points;
    }

    

    private Vector2 getLowest(Vector2[] indexes, Func<Vector2, float> predicate) {
        Vector2 smallest = Vector2.down;
        float min = Int32.MaxValue;
        foreach (var index in indexes) {
            var indexValue = predicate.Invoke(index);
            if (indexValue < min) {
                min = indexValue;
                smallest = index;
            }
        }

        return smallest;
    }

    private Vector2 getHighest(Vector2[] indexes, Func<Vector2, float> predicate) {
        Vector2 smallest = Vector2.down;
        float max = Int32.MinValue;
        foreach (var index in indexes) {
            var indexValue = predicate.Invoke(index);
            if (indexValue > max) {
                max = indexValue;
                smallest = index;
            }
        }

        return smallest;
    }

    public static IEnumerable<Vector2> GetPointsOnLine(int x0, int y0, int x1, int y1) {
        bool steep = Math.Abs(y1 - y0) > Math.Abs(x1 - x0);
        if (steep) {
            int t;
            t = x0; // swap x0 and y0
            x0 = y0;
            y0 = t;
            t = x1; // swap x1 and y1
            x1 = y1;
            y1 = t;
        }

        if (x0 > x1) {
            int t;
            t = x0; // swap x0 and x1
            x0 = x1;
            x1 = t;
            t = y0; // swap y0 and y1
            y0 = y1;
            y1 = t;
        }

        int dx = x1 - x0;
        int dy = Math.Abs(y1 - y0);
        int error = dx / 2;
        int ystep = (y0 < y1) ? 1 : -1;
        int y = y0;
        for (int x = x0; x <= x1; x++) {
            yield return new Vector2((steep ? y : x), (steep ? x : y));
            error = error - dy;
            if (error < 0) {
                y += ystep;
                error += dx;
            }
        }
    }
}