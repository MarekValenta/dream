using UnityEngine;

public class GameObjectWithComponent<T> {
    public GameObject gameObject;
    public T component;

    public GameObjectWithComponent(GameObject gameObject, T component) {
        this.gameObject = gameObject;
        this.component = component;
    }
}