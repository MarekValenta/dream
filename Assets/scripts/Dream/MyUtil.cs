using System;
using System.Collections.Generic;
using System.Threading;

public class MyUtil {
    public static Dictionary<String, Object> context = new Dictionary<string, object>();
    
    public static BaseBlockProviderGenerator TYPE_GENERATOR = new PerlinNoiseBlockGenerator();
    public const int SIZE_OF_CHUNK = 16;
    public static void forEachBlock(Action<Index> action, int size) {
        for (int x = 0; x < size; x++) {
            for (int z = 0; z < size; z++) {
                for (int y = 0; y < size; y++) {
                    action(Index.getIndex(x, y, z));
                }
            }
        }
    }
    
    public static void forEachXYZ(Action<int, int, int> action, int x, int y, int z) {
        for (int xx = 0; xx < x; xx++) {
            for (int zz = 0; zz < z; zz++) {
                for (int yy = 0; yy < y; yy++) {
                    action(xx,yy,zz);
                }
            }
        }
    }

    public static void forEachBlockOnThePlane(Func<Index, bool> action, int size) {
        bool isContinue = true;
        for (int x = 0; x < size; x++) {
            for (int z = 0; z < size; z++) {
                isContinue = action(Index.getIndex(x, 0, z));
                if (!isContinue) {
                    break;
                }
            }
            if (!isContinue) {
                break;
            }
        }
    }
    
    public static void forEachBlockOnThePlane(Action<Index> action, int size) {
        for (int x = 0; x < size; x++) {
            for (int z = 0; z < size; z++) {
                action(Index.getIndex(x, 0, z));
            }
        }
    }

    public static T[] forEachBlock<T>(Func<Index, T> action, int size) {
        List<T> returnList = new List<T>();
        for (int x = 0; x < size; x++) {
            for (int z = 0; z < size; z++) {
                for (int y = 0; y < size; y++) {
                    var action1 = action(Index.getIndex(x, y, z));
                    if (action1 != null) {
                        returnList.Add(action1);
                    }
                }
            }
        }

        return returnList.ToArray();
    }
    
    public static int hash(int x, int y, int z) {
        if (x == 0 && y == 0 && z == 0) {
            return Int32.MaxValue;
        }

        return (((x << 8) ^ y) << 8) ^ z;
    }

    public static T addToContext<T>(Type type) {
        var instance = (T)Activator.CreateInstance(type);
        context.Add(type.Name, instance);
        return instance;
    }
}