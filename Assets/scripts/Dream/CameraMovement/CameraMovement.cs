﻿using System;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
    public float speed = 20f;
    public float spacerDistce = 20f;
    [Range(0.01f, 1f)] public float smoothingFactor = 0.5f;
    public float SensitivityX;
    public float SensitivityY;
    public int heightLimit = 150;
    
    private void Start() {
    }

    // Update is called once per frame
    void Update() {
        var transform1 = transform;
        Physics.Raycast(transform1.position, transform1.forward, out var hit, 1000);

        if (Input.GetMouseButton(2)) {
            var transformPositionForRotation = transform1.position; 
            Vector3 offset = transformPositionForRotation - hit.point;
            Quaternion camToRotateX = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * SensitivityX, transform1.up);

            Quaternion camToRotateY = new Quaternion(0, 0, 0, 1);
            if (Input.GetAxis("Mouse Y") > 0 && transform1.eulerAngles.x > 50) {
                camToRotateY = Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * -SensitivityX, transform1.right);
            } else if (Input.GetAxis("Mouse Y") < 0 && transform1.eulerAngles.x < 85 ) {
                camToRotateY = Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * -SensitivityX, transform1.right);
            }

            offset = camToRotateX * camToRotateY * offset;
            Vector3 pos2 = hit.point + offset;
            transform1.position = Vector3.Slerp(transformPositionForRotation, pos2, smoothingFactor);
            transform1.LookAt(hit.point);
        }

        Vector3 pos = transform1.position;

        var transform1Forward = transform1.forward;
        var wPost = new Vector3(transform1Forward.x, 0, transform1Forward.z).normalized;
        
        if (Input.GetKey("w")) {
            pos += wPost * (speed * Time.deltaTime);
        }

        if (Input.GetKey("s")) {
            pos -= wPost * (speed * Time.deltaTime);
        }

        if (Input.GetKey("d")) {
            pos += transform1.right * (speed * Time.deltaTime);
        }

        if (Input.GetKey("a")) {
            pos -= transform1.right * (speed * Time.deltaTime);
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (Vector3.Distance(hit.point, transform1.position ) > spacerDistce && scroll < 0) {
            scroll = Math.Max(scroll, -2);
            pos += transform1.forward * (scroll * speed * 25f * Time.deltaTime);
        }

        if (pos.y < heightLimit - 1) {
            scroll = Math.Min(scroll, 2);
            pos += transform1.forward * (scroll * speed * 25f * Time.deltaTime);
        }

        if (Vector3.Distance(hit.point, transform1.position ) < spacerDistce) {
            pos.y += speed * Time.deltaTime;
        }


        if (pos.y > heightLimit) {
            pos.y = heightLimit;
        }

        transform1.position = Vector3.Slerp(transform1.position, pos, smoothingFactor);
    }
}