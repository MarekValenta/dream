﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpacerFromTerain : MonoBehaviour {
    public bool isColliding;

    private void OnTriggerEnter(Collider other) {
        isColliding = true;
        Debug.Log("Enter");
    }

    private void OnTriggerExit(Collider other) {
        isColliding = false;
        Debug.Log("Exit");
        
    }
}
