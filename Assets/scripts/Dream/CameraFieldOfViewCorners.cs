using UnityEngine;
using static MyUtil;

public class CameraFieldOfViewCorners{

    private readonly Camera camera;
    private int mask;

    public CameraFieldOfViewCorners(Camera camera, int mask) {
        this.camera = camera;
        this.mask = mask;
    }

    public Index[] getCorners() {
        Ray topLeft = camera.ViewportPointToRay(new Vector3(0, 0, 0));
        Ray topRight = camera.ViewportPointToRay(new Vector3(1, 0, 0));
        Ray botRight = camera.ViewportPointToRay(new Vector3(1, 1, 0));
        Ray botLeft = camera.ViewportPointToRay(new Vector3(0, 1, 0));

        Physics.Raycast(botLeft, out var hit, 1000, mask);
        Vector3 topLeftCorner = hit.point / SIZE_OF_CHUNK;

        Physics.Raycast(botRight, out var hit2, 1000, mask);
        Vector3 topRightCorner = hit2.point / SIZE_OF_CHUNK;

        Physics.Raycast(topLeft, out var hit3, 1000, mask);
        Vector3 botLeftCorner = hit3.point / SIZE_OF_CHUNK;

        Physics.Raycast(topRight, out var hit4, 1000, mask);
        Vector3 botRightCorner = hit4.point / SIZE_OF_CHUNK;

        var centerBefore = (topLeftCorner + topRightCorner + botLeftCorner + botRightCorner) / 4;

        var scale = 2;
        var botLeftCornerTep = (botLeftCorner - centerBefore).normalized;
        botLeftCorner += botLeftCornerTep * scale;

        var botRightCornerTemp = (botRightCorner - centerBefore).normalized;
        botRightCorner += botRightCornerTemp * scale;
        
        var topRightCornerTemp = (topRightCorner - centerBefore).normalized;
        topRightCorner += topRightCornerTemp * scale;
        
        var topLeftCornerTemp = (topLeftCorner - centerBefore).normalized;
        topLeftCorner += topLeftCornerTemp * scale;


        return new[] {Index.@from(botLeftCorner), Index.@from(botRightCorner), Index.@from(topRightCorner), Index.@from(topLeftCorner)};
    }
}