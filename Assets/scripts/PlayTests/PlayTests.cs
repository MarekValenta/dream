﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using Unity.Collections;
using Unity.Jobs;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;
using Object = UnityEngine.Object;

namespace PlayTests {
    public class PlayTests {
        // A Test behaves as an ordinary method


        [UnityTest]
        // [Timeout(3000)]
        public IEnumerator oneSimpleChunkTest() {
            var chunkPrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Resources/Prefabs/Chunk.prefab");
            var areaLight = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Resources/Prefabs/AreaLight.prefab");
            Object.Instantiate(areaLight);

            ChunkHeightGenerator chunkHeightGenerator = new ChunkHeightGenerator();
            var indexOfChunk = Index.getIndex(0, 0, 0);
            var generateHeights = chunkHeightGenerator.generateHeights(MyUtil.TYPE_GENERATOR, indexOfChunk);

            var chunkGameObject = Object.Instantiate(chunkPrefab, Vector3.zero, Quaternion.identity);
            var chunkComponent = chunkGameObject.GetComponent<ChunkComponent>();

            Chunk chunk = new Chunk(MyUtil.TYPE_GENERATOR, indexOfChunk, generateHeights);

            chunkComponent.init(chunk);

            yield return null;
            // yield return new WaitForSeconds(5);
        }

        [UnityTest]
        // [Timeout(3000)]
        public IEnumerator verticalChunksTest() {
            var chunkPrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Resources/Prefabs/Chunk.prefab");
            var areaLight = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Resources/Prefabs/AreaLight.prefab");
            Object.Instantiate(areaLight);

            ChunkHeightGenerator chunkHeightGenerator = new ChunkHeightGenerator();
            var indexOfChunk = Index.getIndex(0, 0, 0);
            var generateHeights = chunkHeightGenerator.generateHeights(MyUtil.TYPE_GENERATOR, indexOfChunk);

            for (int y = -2; y < 6; y++) {
                indexOfChunk = Index.getIndex(0, y, 0).multiply(16);
                var chunkGameObject = Object.Instantiate(chunkPrefab, indexOfChunk.multiply(16).vector3(), Quaternion.identity);
                var chunkComponent = chunkGameObject.GetComponent<ChunkComponent>();

                Chunk chunk = new Chunk(MyUtil.TYPE_GENERATOR, indexOfChunk, generateHeights);

                chunkComponent.init(chunk);
            }
            yield return null;
            // yield return new WaitForSeconds(5);
        }

        [UnityTest]
        // [Timeout(3000)]
        public IEnumerator loadChunksTest() {
            var chunkPrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Resources/Prefabs/Chunk.prefab");
            var areaLight = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Resources/Prefabs/AreaLight.prefab");
            Object.Instantiate(areaLight);

            ChunkHeightGenerator chunkHeightGenerator = new ChunkHeightGenerator();
            var xSize = 10;
            var zSize = 10;
            for (int x = 0; x < xSize; x++) {
                for (int z = 0; z < zSize; z++) {
                    var indexOfChunk = Index.getIndex(x, 0, z).multiply(16);
                    var generateHeights = chunkHeightGenerator.generateHeights(MyUtil.TYPE_GENERATOR, indexOfChunk);

                    for (int y = -2; y < 6; y++) {
                        indexOfChunk = Index.getIndex(x, y, z).multiply(16);
                        var chunkGameObject = Object.Instantiate(chunkPrefab, indexOfChunk.vector3(), Quaternion.identity);
                        var chunkComponent = chunkGameObject.GetComponent<ChunkComponent>();

                        Chunk chunk = new Chunk(MyUtil.TYPE_GENERATOR, indexOfChunk, generateHeights);

                        chunkComponent.init(chunk);
                    }
                }
            }

            yield return null;
            // yield return new WaitForSeconds(5);
        }
    }
}